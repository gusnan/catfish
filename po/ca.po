# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Xfce Bot <transifex@xfce.org>, 2018
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2020
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-01 13:36-0500\n"
"PO-Revision-Date: 2018-06-28 22:08+0000\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2020\n"
"Language-Team: Catalan (https://www.transifex.com/xfce/teams/16840/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../org.xfce.Catfish.desktop.in.h:1 ../data/ui/CatfishWindow.ui.h:28
#: ../catfish_lib/catfishconfig.py:88
msgid "Catfish File Search"
msgstr "Cerca de fitxers Catfish"

#: ../org.xfce.Catfish.desktop.in.h:2
msgid "File search"
msgstr "Cerca de fitxers"

#: ../org.xfce.Catfish.desktop.in.h:3
msgid "Search the file system"
msgstr "Cerqueu al sistema de fitxers"

#. TRANSLATORS: Search terms to find this application. Do NOT translate or
#. localize the semicolons! The list MUST also end with a semicolon!
#: ../org.xfce.Catfish.desktop.in.h:5
msgid "files;find;locate;lookup;search;"
msgstr "fitxers;cercar;buscar;recerca;"

#: ../data/ui/CatfishPreferences.ui.h:1
msgid "Catfish Preferences"
msgstr "Preferències de Catfish"

#: ../data/ui/CatfishPreferences.ui.h:2
msgid "_Close"
msgstr "Tan_ca"

#: ../data/ui/CatfishPreferences.ui.h:3
msgid "Classic (_Titlebar)"
msgstr "Clàssic (Bara de _títol)"

#: ../data/ui/CatfishPreferences.ui.h:4
msgid "_Modern (CSD)"
msgstr "_Modern (CSD)"

#: ../data/ui/CatfishPreferences.ui.h:5
msgid "Your new window layout will be applied after restarting Catfish."
msgstr ""
"La nova disposició de la finestra s'aplicarà després de reiniciar Catfish."

#: ../data/ui/CatfishPreferences.ui.h:6
msgid "Window Layout"
msgstr "Disposició de la finestra"

#: ../data/ui/CatfishPreferences.ui.h:7
msgid "Show _hidden files in the results"
msgstr "Mostra els fitxers _ocults als resultats"

#: ../data/ui/CatfishPreferences.ui.h:8
msgid "Show filter _sidebar"
msgstr "Mostra la barra lateral de filtratge"

#: ../data/ui/CatfishPreferences.ui.h:9
msgid "Display Options"
msgstr "Opcions de visualització"

#: ../data/ui/CatfishPreferences.ui.h:10
msgid "Appearance"
msgstr "Aparença"

#: ../data/ui/CatfishPreferences.ui.h:11
msgid "Path"
msgstr "Camí"

#: ../data/ui/CatfishPreferences.ui.h:12
msgid "Add Directory..."
msgstr "Afegeix el directori..."

#: ../data/ui/CatfishPreferences.ui.h:13
msgid "_Add"
msgstr "_Afegeix"

#: ../data/ui/CatfishPreferences.ui.h:14
msgid "Remove Directory"
msgstr "Suprimeix el directori"

#: ../data/ui/CatfishPreferences.ui.h:15
msgid "_Remove"
msgstr "Sup_rimeix"

#: ../data/ui/CatfishPreferences.ui.h:16
msgid "Exclude Directories"
msgstr "Exclou els directoris"

#: ../data/ui/CatfishPreferences.ui.h:17
msgid "Close the search _window after opening a file"
msgstr "Tanca la _finestra de cerca després d’obrir un fitxer"

#: ../data/ui/CatfishPreferences.ui.h:18
msgid "Miscellaneous"
msgstr "Miscel·lània"

#: ../data/ui/CatfishPreferences.ui.h:19
msgid "Advanced"
msgstr "Avançada"

#: ../data/ui/CatfishWindow.ui.h:1
msgid "_Open"
msgstr "_Obre"

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:3
msgid "Show in _File Manager"
msgstr "_Mostra al gestor de fitxers"

#. This menu contains the menu items: _Open, Show in _File Manager, _Copy
#. Location, Save _As, _Delete
#: ../data/ui/CatfishWindow.ui.h:5
msgid "_Copy Location"
msgstr "_Copia la ubicació"

#: ../data/ui/CatfishWindow.ui.h:6
msgid "_Save as..."
msgstr "_Anomena i desa…"

#: ../data/ui/CatfishWindow.ui.h:7
msgid "_Delete"
msgstr "_Suprimeix"

#: ../data/ui/CatfishWindow.ui.h:8
msgid "File Extensions"
msgstr "Extensions de fitxer"

#: ../data/ui/CatfishWindow.ui.h:9
msgid "odt, png, txt"
msgstr "odt, png i txt"

#: ../data/ui/CatfishWindow.ui.h:10
msgid "Documents"
msgstr "Documents"

#: ../data/ui/CatfishWindow.ui.h:11
msgid "Folders"
msgstr "Carpetes"

#: ../data/ui/CatfishWindow.ui.h:12
msgid "Images"
msgstr "Imatges"

#: ../data/ui/CatfishWindow.ui.h:13
msgid "Music"
msgstr "Música"

#: ../data/ui/CatfishWindow.ui.h:14
msgid "Videos"
msgstr "Vídeos"

#: ../data/ui/CatfishWindow.ui.h:15
msgid "Applications"
msgstr "Aplicacions"

#: ../data/ui/CatfishWindow.ui.h:16
msgid "Other"
msgstr "Altres"

#: ../data/ui/CatfishWindow.ui.h:17
msgid "Any time"
msgstr "En qualsevol moment"

#: ../data/ui/CatfishWindow.ui.h:18
msgid "This week"
msgstr "Aquesta setmana"

#: ../data/ui/CatfishWindow.ui.h:19
msgid "Custom"
msgstr "Personalitza"

#: ../data/ui/CatfishWindow.ui.h:20
msgid "Go to Today"
msgstr "Vés a la data d'avui"

#: ../data/ui/CatfishWindow.ui.h:21
msgid "<b>Start Date</b>"
msgstr "<b>Data inicial</b>"

#: ../data/ui/CatfishWindow.ui.h:22
msgid "<b>End Date</b>"
msgstr "<b>Data final</b>"

#: ../data/ui/CatfishWindow.ui.h:23 ../catfish_lib/Window.py:228
msgid "Catfish"
msgstr "Catfish"

#: ../data/ui/CatfishWindow.ui.h:24
msgid "Update"
msgstr "Actualitza"

#: ../data/ui/CatfishWindow.ui.h:25
msgid "The search database is more than 7 days old.  Update now?"
msgstr "L'índex de cerca té més de 7 dies. Voleu actualitzar-lo ara?"

#: ../data/ui/CatfishWindow.ui.h:26
msgid "File Type"
msgstr "Tipus de fitxer"

#: ../data/ui/CatfishWindow.ui.h:27 ../catfish/CatfishWindow.py:1321
msgid "Modified"
msgstr "Data de modificació"

#: ../data/ui/CatfishWindow.ui.h:29 ../catfish/CatfishWindow.py:1766
msgid "Results will be displayed as soon as they are found."
msgstr "Els resultats es mostraran tan aviat com es trobin."

#: ../data/ui/CatfishWindow.ui.h:30
msgid "Update Search Database"
msgstr "Actualitza l'índex de cerca"

#. Restore Cancel button
#. Buttons
#: ../data/ui/CatfishWindow.ui.h:31 ../catfish/CatfishWindow.py:716
#: ../catfish_lib/SudoDialog.py:193
msgid "Cancel"
msgstr "Cancel·la"

#: ../data/ui/CatfishWindow.ui.h:32
msgid "Unlock"
msgstr "Desbloqueja"

#: ../data/ui/CatfishWindow.ui.h:33
msgid "<b>Database:</b>"
msgstr "<b>Índex de cerca:</b>"

#: ../data/ui/CatfishWindow.ui.h:34
msgid "<b>Updated:</b>"
msgstr "<b>Actualització:</b>"

#: ../data/ui/CatfishWindow.ui.h:35
msgid "<big><b>Update Search Database</b></big>"
msgstr "<big><b>Actualització de l'índex de cerca</b></big>"

#: ../data/ui/CatfishWindow.ui.h:36
msgid ""
"For faster search results, the search database needs to be refreshed.\n"
"This action requires administrative rights."
msgstr ""
"Per obtenir resultats més ràpid, cal actualitzar l'índex de cerca.\n"
"Aquesta acció requereix permisos d'administrador."

#: ../data/ui/CatfishWindow.ui.h:38
msgid "Select a Directory"
msgstr "Selecciona una carpeta"

#: ../data/ui/CatfishWindow.ui.h:39
msgid "Search for files"
msgstr "Cerca de fitxers"

#: ../data/ui/CatfishWindow.ui.h:40
msgid "Compact List"
msgstr "Llista compacta"

#: ../data/ui/CatfishWindow.ui.h:41
msgid "Thumbnails"
msgstr "Miniatures"

#: ../data/ui/CatfishWindow.ui.h:42
msgid "Show _sidebar"
msgstr "Mostra la barra _lateral"

#: ../data/ui/CatfishWindow.ui.h:43
msgid "Show _hidden files"
msgstr "Mostra els fitxers ocult_s"

#: ../data/ui/CatfishWindow.ui.h:44
msgid "Search file _contents"
msgstr "Cerca al _contingut dels fitxers"

#: ../data/ui/CatfishWindow.ui.h:45
msgid "_Match results exactly"
msgstr "_Coincidència exacta dels resultats"

#: ../data/ui/CatfishWindow.ui.h:46
msgid "_Refresh search index..."
msgstr "_Refresca la base de dades de cerques..."

#: ../data/ui/CatfishWindow.ui.h:47
msgid "_Preferences"
msgstr "_Preferències"

#: ../data/ui/CatfishWindow.ui.h:48
msgid "_About"
msgstr "_Quant a"

#: ../catfish/__init__.py:39
msgid "Usage: %prog [options] path query"
msgstr "Ús: %prog [opcions] camí consulta"

#: ../catfish/__init__.py:44
msgid "Show debug messages (-vv will also debug catfish_lib)"
msgstr "Mostra els missatges de depuració (-vv també depurarà catfish_lib)"

#: ../catfish/__init__.py:47
msgid "Use large icons"
msgstr "Utilitza icones grans"

#: ../catfish/__init__.py:49
msgid "Use thumbnails"
msgstr "Utilitza miniatures"

#: ../catfish/__init__.py:51
msgid "Display time in ISO format"
msgstr "Mostra l'hora en format ISO"

#. Translators: Do not translate PATH, it is a variable.
#: ../catfish/__init__.py:53
msgid "Set the default search path"
msgstr "Estableix el camí de cerca predeterminat"

#: ../catfish/__init__.py:55
msgid "Perform exact match"
msgstr "Busca una coincidència exacta"

#: ../catfish/__init__.py:57
msgid "Include hidden files"
msgstr "Inclou els fitxers ocults"

#: ../catfish/__init__.py:59
msgid "Perform fulltext search"
msgstr "Cerca el text complet"

#: ../catfish/__init__.py:61
msgid ""
"If path and query are provided, start searching when the application is "
"displayed."
msgstr ""
"Si es proporcionen el camí i la consulta, la cerca comença quan es mostra "
"l'aplicació."

#. Translators: this text is displayed next to
#. a filename that is not utf-8 encoded.
#: ../catfish/CatfishWindow.py:106
#, python-format
msgid "%s (invalid encoding)"
msgstr "%s (codif. no vàlida)"

#: ../catfish/CatfishWindow.py:146
msgid "translator-credits"
msgstr ""
"Carles Muñoz Gorriz <carlesmu@internautas.org>, 2009.\n"
"Harald Servat <redcrash@gmail.com>, 2009-2010.\n"
"Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016-2019."

#: ../catfish/CatfishWindow.py:286
msgid "Unknown"
msgstr "Desconeguda"

#: ../catfish/CatfishWindow.py:290
msgid "Never"
msgstr "Mai"

#: ../catfish/CatfishWindow.py:374
#, python-format
msgid ""
"Enter your query above to find your files\n"
"or click the %s icon for more options."
msgstr ""
"Introduïu a dalt una consulta per cercar fitxers\n"
"o feu clic a la icona %s per a més opcions."

#: ../catfish/CatfishWindow.py:731
msgid "An error occurred while updating the database."
msgstr "S'ha produït un error en actualitzar l'índex de cerca."

#: ../catfish/CatfishWindow.py:733
msgid "Authentication failed."
msgstr "Ha fallat l'autenticació."

#: ../catfish/CatfishWindow.py:739
msgid "Authentication cancelled."
msgstr "S'ha cancel·lat l'autenticació."

#: ../catfish/CatfishWindow.py:745
msgid "Search database updated successfully."
msgstr "L'índex de cerca s'ha actualitzat correctament."

#. Update the Cancel button to Close, make it default
#: ../catfish/CatfishWindow.py:806
msgid "Close"
msgstr "Tanca"

#. Set the dialog status to running.
#: ../catfish/CatfishWindow.py:820
msgid "Updating..."
msgstr "S'està actualitzant..."

#: ../catfish/CatfishWindow.py:854
msgid "Stop Search"
msgstr "Atura la cerca"

#: ../catfish/CatfishWindow.py:855
msgid ""
"Search is in progress...\n"
"Press the cancel button or the Escape key to stop."
msgstr ""
"Cerca en curs...\n"
"Premeu el botó de cancel·lació o la tecla Esc per aturar-la."

#: ../catfish/CatfishWindow.py:864
msgid "Begin Search"
msgstr "Comença la cerca"

#: ../catfish/CatfishWindow.py:1142
#, python-format
msgid "\"%s\" could not be opened."
msgstr "No s'ha pogut obrir «%s»."

#: ../catfish/CatfishWindow.py:1194
#, python-format
msgid "\"%s\" could not be saved."
msgstr "No s'ha pogut desar «%s»."

#: ../catfish/CatfishWindow.py:1211
#, python-format
msgid "\"%s\" could not be deleted."
msgstr "No s'ha pogut suprimir «%s»."

#: ../catfish/CatfishWindow.py:1250
#, python-format
msgid "Save \"%s\" as..."
msgstr "Anomena i desa «%s»..."

#: ../catfish/CatfishWindow.py:1285
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete \"%s\"?"
msgstr ""
"Segur que voleu suprimir \n"
"permanentment «%s»?"

#: ../catfish/CatfishWindow.py:1289
#, python-format
msgid ""
"Are you sure that you want to \n"
"permanently delete the %i selected files?"
msgstr ""
"Segur que voleu suprimir \n"
"permanentment els %i fitxers seleccionats?"

#: ../catfish/CatfishWindow.py:1292
msgid "If you delete a file, it is permanently lost."
msgstr "Si suprimiu un fitxer, es perdrà permanentment."

#: ../catfish/CatfishWindow.py:1315
msgid "Filename"
msgstr "Nom"

#: ../catfish/CatfishWindow.py:1317
msgid "Size"
msgstr "Mida"

#: ../catfish/CatfishWindow.py:1319
msgid "Location"
msgstr "Ubicació"

#: ../catfish/CatfishWindow.py:1331
msgid "Preview"
msgstr "Vista prèvia"

#: ../catfish/CatfishWindow.py:1339
msgid "Details"
msgstr "Detalls"

#: ../catfish/CatfishWindow.py:1556
msgid "Today"
msgstr "Avui"

#: ../catfish/CatfishWindow.py:1558
msgid "Yesterday"
msgstr "Ahir"

#: ../catfish/CatfishWindow.py:1639
msgid "No files found."
msgstr "No s'ha trobat cap fitxer."

#: ../catfish/CatfishWindow.py:1641
msgid ""
"Try making your search less specific\n"
"or try another directory."
msgstr ""
"Intenteu fer una cerca menys específica\n"
"o intenteu-ho en una altra carpeta."

#: ../catfish/CatfishWindow.py:1648
msgid "1 file found."
msgstr "S'ha trobat un fitxer."

#: ../catfish/CatfishWindow.py:1650
#, python-format
msgid "%i files found."
msgstr "S'han trobat %i fitxers."

#: ../catfish/CatfishWindow.py:1656
msgid "bytes"
msgstr "bytes"

#: ../catfish/CatfishWindow.py:1764 ../catfish/CatfishWindow.py:1774
msgid "Searching..."
msgstr "S'està cercant..."

#: ../catfish/CatfishWindow.py:1772
#, python-format
msgid "Searching for \"%s\""
msgstr "S'està cercant «%s»"

#: ../catfish/CatfishWindow.py:1861
#, python-format
msgid "Search results for \"%s\""
msgstr "Resultats de la cerca «%s»"

#: ../catfish_lib/catfishconfig.py:91
msgid "Catfish is a versatile file searching tool."
msgstr "Catfish és una eina versàtil per cercar fitxers."

#: ../catfish_lib/SudoDialog.py:135
msgid "Password Required"
msgstr "Es requereix una contrasenya"

#: ../catfish_lib/SudoDialog.py:172
msgid "Incorrect password... try again."
msgstr "Contrasenya incorrecta… Intenteu-ho de nou."

#: ../catfish_lib/SudoDialog.py:182
msgid "Password:"
msgstr "Contrasenya:"

#: ../catfish_lib/SudoDialog.py:196
msgid "OK"
msgstr "D'acord"

#: ../catfish_lib/SudoDialog.py:217
msgid ""
"Enter your password to\n"
"perform administrative tasks."
msgstr ""
"Introduïu la contrasenya per\n"
"realitzar tasques administratives."

#: ../catfish_lib/SudoDialog.py:219
#, python-format
msgid ""
"The application '%s' lets you\n"
"modify essential parts of your system."
msgstr ""
"L'aplicació «%s» us permet\n"
"modificar parts essencials del sistema."

#: ../data/metainfo/catfish.appdata.xml.in.h:1
msgid "Versatile file searching tool"
msgstr "Eina versàtil de cerca de fitxers"

#: ../data/metainfo/catfish.appdata.xml.in.h:2
msgid ""
"Catfish is a small, fast, and powerful file search utility. Featuring a "
"minimal interface with an emphasis on results, it helps users find the files"
" they need without a file manager. With powerful filters such as "
"modification date, file type, and file contents, users will no longer be "
"dependent on the file manager or organization skills."
msgstr ""
"Catfish és una utilitat de cerca de fitxers petita, ràpida i potent. Amb una"
" interfície mínima que destaca els resultats, ajuda els usuaris a trobar els"
" fitxers que necessiten sense recórrer al gestor de fitxers. Amb filtres "
"potents, com ara el de data de modificació, el de tipus de fitxer i el de "
"contingut dels fitxers, els usuaris ja no depenen del gestor de fitxers ni "
"de les seves habilitats d'organització."

#: ../data/metainfo/catfish.appdata.xml.in.h:3
msgid "The main Catfish window displaying image search results"
msgstr ""
"La finestra principal de catfish que mostra els resultats de cerca d'imatges"

#: ../data/metainfo/catfish.appdata.xml.in.h:4
msgid ""
"The main Catfish window displaying available filters and filtered search "
"results"
msgstr ""
"La finestra principal de catfish que mostra els filtres disponibles i els "
"resultats de cerca filtrats"

#: ../data/metainfo/catfish.appdata.xml.in.h:5
msgid ""
"This release adds support for running under Wayland and features refreshed "
"dialogs with simplified controls."
msgstr ""
"Aquesta afegeix la compatibilitat per a l'execució sota Wayland i "
"proporciona diàlegs actualitzats amb controls simplificats."

#: ../data/metainfo/catfish.appdata.xml.in.h:6
msgid ""
"This release addresses some startup errors as well as various issues with "
"traversing symbolic links."
msgstr ""
"Aquest llançament tracta alguns errors d’inici, així com diversos problemes "
"amb el recorregut d'enllaços simbòlics."

#: ../data/metainfo/catfish.appdata.xml.in.h:7
msgid ""
"This release includes many appearance improvements, introduces a new "
"preferences dialog, and improves search results and performance."
msgstr ""
"Aquesta versió inclou moltes millores en l’aparença, afegeix un nou diàleg "
"de preferències i millora els resultats i el rendiment de la cerca."

#: ../data/metainfo/catfish.appdata.xml.in.h:8
msgid ""
"This release features performance improvements by excluding uncommon search "
"directories."
msgstr ""
"Aquesta versió inclou diverses millores de rendiment en excloure directoris "
"poc comuns de cerca."

#: ../data/metainfo/catfish.appdata.xml.in.h:9
msgid ""
"This release features better desktop integration, support for OpenBSD and "
"Wayland, and improved translation support."
msgstr ""
"Aquesta versió destaca una millor integració d’escriptori, un millor suport "
"per a OpenBSD i Wayland i suport millorat per a les traduccions."

#: ../data/metainfo/catfish.appdata.xml.in.h:10
msgid ""
"This release features improved performance, better desktop integration, and "
"a number of long-standing bugs. Quoted search strings are now correctly "
"processed. Files can be dragged into other applications. Thumbnails are only"
" generated when requested by the user."
msgstr ""
"Aquest llançament ofereix un rendiment millorat, una millor integració "
"d'escriptori i una sèrie d'errors de llarga durada. Les cadenes de cerca "
"entre cometes ara es processen correctament. Els fitxers es poden arrossegar"
" a altres aplicacions. Les miniatures només es generen quan l'usuari ho "
"sol·licita."

#: ../data/metainfo/catfish.appdata.xml.in.h:11
msgid ""
"This release features several improvements to thumbnail processing and "
"numerous bug fixes. Icon previews have been improved and will now match "
"other applications. Items displayed at the bottom of the results window are "
"now accessible with all desktop themes."
msgstr ""
"Aquest llançament ofereix diverses millores al processament de miniatures i "
"nombroses correccions d'errors. S'han millora les vistes prèvies de les "
"icones i ara coincideixen amb altres aplicacions. Els elements que es "
"mostren a la part inferior de la finestra de resultats ara són accessibles "
"amb tots els temes d'escriptori."

#: ../data/metainfo/catfish.appdata.xml.in.h:12
msgid "This is a minor translations-only release."
msgstr "Aquesta és una versió menor només per a traduccions."

#: ../data/metainfo/catfish.appdata.xml.in.h:13
msgid ""
"This release features several performance improvements and numerous "
"translation updates."
msgstr ""
"Aquesta versió inclou diverses millores de rendiment i moltes "
"actualitzacions en les traduccions."

#: ../data/metainfo/catfish.appdata.xml.in.h:14
msgid ""
"This release now displays all file timestamps according to timezone instead "
"of Universal Coordinated Time (UTC)."
msgstr ""
"Aquesta versió mostra ara totes les marques de temps dels fitxers d'acord "
"amb la zona horària en lloc de amb el temps universal coordinat (UTC)."

#: ../data/metainfo/catfish.appdata.xml.in.h:15
msgid ""
"This release fixes several bugs related to the results window. Files are "
"once again removed from the results list when deleted. Middle- and right-"
"click functionality has been restored. Date range filters are now applied "
"according to timezone instead of Universal Coordinated Time (UTC)."
msgstr ""
"Aquesta versió corregeix diversos errors relacionats amb la finestra de "
"resultats. Els fitxers s'eliminen de la llista de resultats quan se "
"suprimeixen. S'ha restaurat la funcionalitat del botó central i del botó "
"dret del ratolí. El filtre de data de modificació s'aplica ara d'acord amb "
"la zona horària en lloc de amb el temps universal coordinat (UTC)."

#: ../data/metainfo/catfish.appdata.xml.in.h:16
msgid ""
"This release includes a significant interface refresh, improves search "
"speed, and fixes several bugs. The workflow has been improved, utilizing "
"some of the latest features of the GTK+ toolkit, including optional "
"headerbars and popover widgets. Password handling has been improved with the"
" integration of PolicyKit when available."
msgstr ""
"Aquesta versió inclou una actualització significativa de la interfície, "
"millora la velocitat de cerca i corregeix diversos errors. El flux de "
"treball s'ha millorat mitjançant l'ús d'algunes de les últimes "
"característiques del conjunt d'eines GTK+, incloent les barres de capçalera "
"opcionals i els elements gràfics emergents. La gestió de contrasenyes s'ha "
"millorat amb la integració de PolicyKit quan estigui disponible."

#: ../data/metainfo/catfish.appdata.xml.in.h:17
msgid "This release fixes two new bugs and includes updated translations."
msgstr ""
"Aquesta versió corregeix dos errors nous i inclou traduccions actualitzades."

#: ../data/metainfo/catfish.appdata.xml.in.h:18
msgid ""
"This release fixes a regression where the application is unable to start on "
"some systems."
msgstr ""
"Aquesta versió corregeix una regressió que impedia que l'aplicació s'iniciés"
" en alguns sistemes."

#: ../data/metainfo/catfish.appdata.xml.in.h:19
msgid ""
"This release fixes a regression where multiple search terms were no longer "
"supported. An InfoBar is now displayed when the search database is outdated,"
" and the dialogs used to update the database have been improved."
msgstr ""
"Aquesta versió corregeix una regressió que impedia especificar diversos "
"termes de cerca. Ara es mostra una barra d'informació quan l'índex de cerca "
"és obsolet, també s'han millorat els diàlegs per actualitzar la base de "
"dades."

#: ../data/metainfo/catfish.appdata.xml.in.h:20
msgid ""
"This release fixes two issues where locate would not be properly executed "
"and improves handling of missing symbolic icons."
msgstr ""
"Aquesta versió corregeix dos problemes que impedien executar l'ordre locate "
"correctament i millora la gestió de la manca d'icones simbòliques."

#: ../data/metainfo/catfish.appdata.xml.in.h:21
msgid ""
"This stable release improved the reliability of the password dialog, cleaned"
" up unused code, and fixed potential issues with the list and item "
"selection."
msgstr ""
"Aquesta versió estable millora la fiabilitat del diàleg de la contrasenya, "
"neteja el codi sense ús i corregeix possibles incidències amb la llista i la"
" selecció d'elements."

#: ../data/metainfo/catfish.appdata.xml.in.h:22
msgid ""
"This release fixed a potential security issue with program startup and fixed"
" a regression with selecting multiple items."
msgstr ""
"Aquesta actualització soluciona un possible problema de seguretat amb "
"l'inici del programa i corregeix una regressió en seleccionar múltiples "
"elements."

#: ../data/metainfo/catfish.appdata.xml.in.h:23
msgid ""
"The first release in the 1.0.x series introduced a refreshed interface and "
"fixed a number of long-standing bugs. Improvements to the default "
"permissions eliminated a number of warnings when packaging for "
"distributions. Accessibility was enhanced as all strings have been made "
"translatable and keyboard accelerators have been improved."
msgstr ""
"La primera versió de la sèrie 1.0.x introdueix una interfície renovada i "
"corregeix una sèrie d'errors antics. Les millores en els permisos "
"predeterminats han eliminat una sèrie d'advertències en empaquetar per a les"
" distribucions. L'accessibilitat s'ha millorat, ja que totes les cadenes "
"s'han fet traduïbles, i s'han millorat les tecles de drecera."
